# Eight Portions

[![build status](https://gitlab.com/rtlee/rtlee.gitlab.io/badges/master/build.svg)](https://gitlab.com/rtlee/rtlee.gitlab.io/commits/master)
[![coverage report](https://gitlab.com/rtlee/rtlee.gitlab.io/badges/master/coverage.svg)](https://gitlab.com/rtlee/rtlee.gitlab.io/commits/master)

## About
[Eight Portions](https://eightportions.com) is a blog about risk, finance, technology and assorted other topics. It seeks to take a data-driven approach to answering key questions about these topics using open source software and public data where possible.

## Resources
This website was built using the [Beautiful Jekyll](http://deanattali.com/beautiful-jekyll/) template. Additional resources include:
* [Publishing R Markdown using Jekyll](https://chepec.se/2014/07/16/knitr-jekyll.html)
* [Namecheap](https://www.namecheap.com/)
