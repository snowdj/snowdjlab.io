# Next steps

## Site enhancements
* [x] Add picture to landing page
* [ ] Fix JS load (MathJax) on corporate network
* [ ] Add picture to about page
* [ ] Add captions to big imgs
* [ ] Link posts to github repo (reproducible)
  * [ ] Taxi
  * [ ] DJDI
* [ ] Add technologies section to about
* [ ] Edit tags (e.g., "transportation") in `_source` and `_posts`
* [ ] Set up RSS feed
* [ ] Add site to [Jekyll sites wiki](https://github.com/jekyll/jekyll/wiki/sites)
* [x] Add share buttons - see minimal mistakes
* [ ] Enhance 404
  * [ ] Example [here](http://codepen.io/koenigsegg1/pen/VawWov) written in Jade
  * [x] Add navigation buttons
* [ ] Restore navbar to fixed for non-posts
* [ ] Dynamic width navbar (i.e., in-post zoom out)
* [ ] Add pictures to blog posts
* [ ] Create a logo

## Ideas for posts
* Taxi ripoffs
* Unit insurance - rideshare
* Moral hazard in third party insurance
  * Credit insurance
  * Celebrity endorsement insurance
  * Other, e.g., WC, AL
* NFL data - concussions, impact on worker's comp, etc.

## Other projects
