---
layout: page
title: About
bigimg: /img/Trunk.jpg
headerUP: true
---
# About Eight Portions
Eight Portions is a blog about risk, finance, technology and assorted other topics. It seeks to take a data-driven approach to answering key questions about these topics using open-source software and public data where possible.

I'll be using this blog as an opportunity to learn new technical things (e.g., R, git, web basics) and to learn new things about the world (which I'll share in my posts), so please feel free to leave any comments you think would be helpful. Also, don't hesitate to suggest new topics using the email link at the bottom of the site.

> Give a portion to seven, or even to eight,
>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;for you know not what disaster may happen on earth.
>
> -- Ecclesiastes 11:2

# About the author
I am an insurance professional living and working in New York City. I'm a native Californian with an academic background in economics and computer science. Prior to working in the insurance industry, I was a management consultant to the financial services industry.

I dream of making work and life more efficient; I love finding truth as revealed by data, logic and literature; and I believe simplicity is the ultimate sophistication.

In my free time I enjoy traveling, running, skiing, surfing, longboarding, playing video games, playing guitar, playing squash, playing spikeball, and learning new things.
